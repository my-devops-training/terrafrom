# Terraform Intro

## What is Terraform?

* An Infrastructure as code (IaS) tool
* It is a CLI tool
* It compares the configuration file (.tf) and the state file
* It configured using HCL (hashicorp configuration language)
* HCL is like JSON but is not JSON
* It is important to place the config files in a direcotry and all config files must have .tf extension

## Up and Running

### Install Terraform

* Download the terraform from [terraform.io](https://www.terraform.io/downloads.html)
* Move the terraform binary file to /usr/local/bin

**Note** You can use docker instead.
### Running terraform
* Download the terraform providers
```bash
terraform init
```
* Compare the configuration file and state file
```bash
terraform plan
```
* Apply the plan
```bash
terraform apply
```